package com.ldi

import org.apache.flink.api.scala._

/**
 * TP module 5 exercise 5.4
 * Flink Dataset API
 * https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/batch/#example-program
 * Version: 25/11/20 XVA
 */

object WordCountDataSet54 {

  def main(args: Array[String]) {

    val env = ExecutionEnvironment.getExecutionEnvironment
    val text = env.fromElements(
      "Who's there?",
      "I think I hear them. Stand, ho! Who's there?")

    val counts = text.flatMap { _.toLowerCase.split("\\W+") filter { _.nonEmpty } }
      .map { (_, 1) }
      .groupBy(0)
      .sum(1)

    counts.print()
  }

}
