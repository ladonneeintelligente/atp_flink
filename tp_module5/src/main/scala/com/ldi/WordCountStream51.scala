package com.ldi

import org.apache.flink.streaming.api.scala._


/* Env: Scala 2.12 / Flink 1.11.2
* Prerequisites: start in WSL nc -lk 6666
* Enter text in WSL console
*   */

object WordCountStream51 {
  def main(args: Array[String]): Unit = {
    // Get the local operating environment
    val env: StreamExecutionEnvironment = StreamExecutionEnvironment.getExecutionEnvironment
    // Define the source of the socket
    val text: DataStream[String] = env.socketTextStream( hostname="localhost", port = 6666)

    // Scala development needs to add a line of implicit conversion, otherwise it will report an error when calling operator,
    // the role is to find TypeInformation of scala type
    import org.apache.flink.api.scala._

    // Define operators, the function is to parse data, group, and find wordCount
    val wordCount: DataStream[(String, Int)] = text.flatMap(_.split(" ")).map((_,1)).keyBy(_._1).sum( position = 1)

    // Define sink, print data to console
    wordCount.print()

    // Define the name of the task and run
    // Note: operator is lazy, only execute when it encounters execute
    env.execute(jobName = "DataStream WordCount TP exercice 5.2")
  }
}
