package com.ldi

import org.apache.flink.api.scala.createTypeInformation
import org.apache.flink.streaming.api.scala.StreamExecutionEnvironment
import org.apache.flink.streaming.api.windowing.time.Time

/* TP EXERCISE 5.3
* Follow instructions here:
* https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/datastream_api.html#example-program
* Before running start in WSL nc -lk 9999
*  */

object WordCountWindowStream53 {
  def main(args: Array[String]) {

    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val text = env.socketTextStream("localhost", 9999)

    val counts = text.flatMap { _.toLowerCase.split("\\W+") filter { _.nonEmpty } }
      .map { (_, 1) }
      .keyBy(_._1)
      .timeWindow(Time.seconds(5))
      .sum(1)
    counts.print()

    env.execute("TP exercice 5.3")
  }
}
